import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoadingState } from 'src/app/shared/Models/enum';
import { User } from 'src/app/shared/Models/intrerface';
import { UserService } from 'src/app/shared/Services/user.service';
import { AlertController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  form: FormGroup;
  state: LoadingState = LoadingState.LOADING;
  user: User;
  sexList: string[] = ['Hombre', 'Mujer'];

  constructor(private userService: UserService,
    private formBuilder: FormBuilder) {
      this.form = this.formBuilder.group({
        name:  ['', Validators.required],
        birthDate: ['', Validators.required],
        sex: ['', Validators.required],
        phone: ['', Validators.required],
        email: ['', Validators.required]
      });
    }

  ngOnInit(): void {
    this.userService.getUser()
    .subscribe( (user) => {
      this.user = user[0];
      this.state = LoadingState.LOADED;
    },
    error => {
      this.state = LoadingState.ERROR;
    });
  }

  submitForm(){
  }

}
