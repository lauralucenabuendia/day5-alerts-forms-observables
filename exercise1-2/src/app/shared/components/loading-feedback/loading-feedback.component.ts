import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { LoadingState } from '../../Models/enum';

@Component({
  selector: 'app-loading-feedback',
  templateUrl: './loading-feedback.component.html',
  styleUrls: ['./loading-feedback.component.scss'],
})
export class LoadingFeedbackComponent {
  @Input() state: LoadingState.LOADING | LoadingState.ERROR | LoadingState.LOADED;
  @Output() retry = new EventEmitter<void>();

  retryRequest() {
    this.retry.emit();
  }

}
