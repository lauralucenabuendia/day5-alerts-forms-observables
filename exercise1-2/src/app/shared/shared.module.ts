import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

// Componente Loading Feedback
import { LoadingFeedbackComponent } from './components/loading-feedback/loading-feedback.component';


@NgModule({
  declarations: [LoadingFeedbackComponent],
  imports: [ CommonModule, FormsModule, IonicModule],
  exports: [ CommonModule, FormsModule, IonicModule, LoadingFeedbackComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SharedModule { }
